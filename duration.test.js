const getSubArrayMaxLength = require('./getSubArrayMaxLength');

test('a lot of data', () => {
    const array = [];
    const difficulty = 1000000;
    let summ = 0;    

    for (var i = 0; i<difficulty; i++) {
        const newItem = i * 7;
        summ += newItem;
        array.push(newItem);        
    }
    array.reverse();
    valueToCompare = (summ * 2) / 3;

    const utcStart = Date.now();
    const result = getSubArrayMaxLength(array, valueToCompare);
    const duration = Date.now() - utcStart;
    
    expect(result).toBe(816496);
    expect(duration).toBeLessThan(200);
});

test('a lot of data2', () => {
    const array = [];
    const difficulty = 1000000;
    let summ = 0;    

    for (var i = 0; i<difficulty; i++) {
        const newItem = i * 7;
        summ += newItem;
        array.push(newItem);        
    }
    array.reverse();
    valueToCompare = summ + 5;

    const utcStart = Date.now();
    const result = getSubArrayMaxLength(array, valueToCompare);
    const duration = Date.now() - utcStart;
    
    expect(result).toBe(1000000);
    expect(duration).toBeLessThan(200);
});