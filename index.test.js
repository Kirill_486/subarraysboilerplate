//require("babel-register");
// import getSubArrayMaxLength from './index'

const getSubArrayMaxLength = require('./index');

test('getSubArrayMaxLength', () => {
    expect(getSubArrayMaxLength).toBeDefined();
});

test('subarrayTest 4321_3', () => {
    const array = [4, 3, 2, 1];
    const valueToCompare = 3;
    const result = getSubArrayMaxLength(array, valueToCompare);
    expect(result).toBe(2);
});

test('subarrayTest 43121_3', () => {
    const array = [4, 3, 1, 2, 1];
    const valueToCompare = 3;
    const result = getSubArrayMaxLength(array, valueToCompare);
    expect(result).toBe(2);
});

test('subarrayTest 43121_4', () => {
    const array = [4, 3, 1, 2, 1];
    const valueToCompare = 4;
    const result = getSubArrayMaxLength(array, valueToCompare);
    expect(result).toBe(3);
});

test('subarrayTest more then items at all', () => {
    const array = [2, 2, 1, 2, 2];
    const valueToCompare = 10;
    const result = getSubArrayMaxLength(array, valueToCompare);
    expect(result).toBe(5);
});

test('subarrayTest 4321_6', () => {
    const array = [4, 3, 2, 1];
    const valueToCompare = 6;
    const result = getSubArrayMaxLength(array, valueToCompare);
    expect(result).toBe(3);
});

